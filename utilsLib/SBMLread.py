##  Contributors : 
##  Alice Julien-Laferriere
##  Delphine Parrot
##  
##  alice.julien.laferriere@gmail.com
##  delphine.parrot@gmail.com
##
##This software is the preprocessing step for MultiPus
#This software is governed by the CeCILL license under French law and
##abiding by the rules of distribution of free software.  You can  use, 
##modify and/ or redistribute the software under the terms of the CeCILL
##license as circulated by CEA, CNRS and INRIA at the following URL
##"http://www.cecill.info". 
##
##As a counterpart to the access to the source code and  rights to copy,
##modify and redistribute granted by the license, users are provided only
##with a limited warranty  and the software's author,  the holder of the
##economic rights,  and the successive licensors  have only  limited
##liability. 
##
##In this respect, the user's attention is drawn to the risks associated
##with loading,  using,  modifying and/or developing or reproducing the
##software by the user in light of its specific status of free software,
##that may mean  that it is complicated to manipulate,  and  that  also
##therefore means  that it is reserved for developers  and  experienced
##professionals having in-depth computer knowledge. Users are therefore
##encouraged to load and test the software's suitability as regards their
##requirements in conditions enabling the security of their systems and/or 
##data to be ensured and,  more generally, to use and operate it in the 
##same conditions as regards security. 
##
##The fact that you are presently reading this means that you have had
##knowledge of the CeCILL license and that you accept its terms.

from Metabolite import MyMetabolite
from Reaction import MyReaction
from myNetwork import MyNetwork
from libsbml import *
from utils import readFileInList, isTheSameR
import re

 

 ## Returns a tupple (metabolites, reactions, index)   
 ## where metabolites is map (dict) with key  :id metabolite, value: an object Metabolite
 ## where Reactions is map (dict) with key  :id reaction, value: an object Reaction
 ## where index is map (dict) with key : idN Metabolite , value : id Metabolite => TODO : we can get rid of this 
def readSBML(fileName,cofactorslist, weightR, nameNetwork): 
    #------------------------#
    #  Opening&Check SBML    #
    #------------------------#
    print "opening : " + fileName    
    reader = SBMLReader()
    mySBMLDoc = reader.readSBML(fileName)   
    errors = mySBMLDoc.getNumErrors() # not necessary
    if( errors > 0):
        print(" validation error(s): " + str(errors));
        for i in range(0, errors):
            myError =  mySBMLDoc.getError( i )
  
    counterCofactor = {}
    sbmlModel = mySBMLDoc.getModel()
   

    theNetwork = MyNetwork( nameNetwork)     
    #------------------------#
    #      Metabolites       #
    #------------------------#
    index =  dict() # map will contain as key : the name of the met, as value : the id (int)    of the met
    for specie in sbmlModel.getListOfSpecies(): #recherche dans la section species
        if not isItACofactor( specie.getId(), cofactorslist):
            ## Creation of the metabolite
            currentMeta = MyMetabolite( theNetwork.getNextIdM(),  specie.getId(), specie.getName()  )       
            # Storing the metabolite or the id
            index[ specie.getId() ] = currentMeta.getId()
            theNetwork.addMetabolite( currentMeta )
        else :
            counterCofactor [ specie.getId() ] = 0   
     #------------------------#
    #       Reactions        #
    #------------------------#
     # reactions: list of map
     # each map : id, name, idSubstrats, idProducts 
    countRev = 0
    countTheSame = 0
    for reaction in sbmlModel.getListOfReactions(): # cible la section reaction dans le fichier SBML
        idnt = reaction.getId() 
        name =  reaction.getName()
        idSubstrats = [] # liste des idSubstrats    
        isReversible = reaction.getReversible()    
        for sub in reaction.getListOfReactants():   
            if not isItACofactor(sub.getSpecies() , cofactorslist): 
                idSubstrats += [index[ sub.getSpecies() ]]
            else : 
                counterCofactor [ sub.getSpecies() ] += 1


        idProducts = [] #list of products (ids)
        for prod in reaction.getListOfProducts():
            if not isItACofactor(prod.getSpecies(), cofactorslist):
                idProducts += [ index[ prod.getSpecies()] ]               
            else : 
                counterCofactor [ prod.getSpecies() ] += 1
        #if idReac == 30:
         #   print idProducts
          #  print idSubstrats
        # creating forward reaction
        # we need to check if the reaction is not already there
        # take one metabolites
        currentReact = MyReaction( theNetwork.getNextIdR(), idnt, name,   weightR ,idSubstrats, idProducts)         
        if not reactionAlreadyIN(currentReact, theNetwork.getMetabolites(), theNetwork.getReactions() ):
            theNetwork.addReaction(currentReact)           
        else :
            countTheSame +=1
        # if reversible, creating the reverse reaction
        if isReversible : 
            currentReactRev = MyReaction(theNetwork.getNextIdR() ,  idnt + "_rev", name + "rev", weightR , idProducts, idSubstrats)   
            ## NEED TO UPDATE SUBSTRATE AND PRODUCTS
            if not  reactionAlreadyIN(currentReactRev, theNetwork.getMetabolites(), theNetwork.getReactions() ): 
                theNetwork.addReaction(currentReactRev)
                countRev  +=1
            else :
                countTheSame += 1

    print "nb reactions : " + str(theNetwork.getNbReactions()) + " and metabolites : " + str(theNetwork.getNbMetabolites() ) 
    print "nb reactions withoutRev = "  + str(theNetwork.getNbReactions() -  countRev) +  " with Rev :  " + str( countRev )
    print "nb of already seen reactions : " + str( countTheSame)
    print " =================================="
    return theNetwork


## Check if the current metabolite (named nameSpecie) is a cofactor (or iso-enzyme , protein...).
# Return a boolean
def isItACofactor( nameSpecie, cofactorslist ):   
    isCofactor = nameSpecie in cofactorslist    
    return isCofactor

# Check if the reaction is not already present. Returns a boolean
def reactionAlreadyIN( currentR, metabolites, allReactions):
    isTheSame = False
    if len( currentR.getSubstrates() ) > 0  : # avoid uptake reactions 
            # if the reaction is already present, the results will be the same for all subtrates and products            
            testM = metabolites[currentR.getSubstrates()[0]]
            #print testM
            reactionsToTest = [allReactions[k]  for k in testM.getReactOut()]
            if len(reactionsToTest) >0:
                for reac in reactionsToTest : 
                    isTheSame = isTheSame or isTheSameR( currentR, reac) # if already seens one, we dont care
    return isTheSame




        
