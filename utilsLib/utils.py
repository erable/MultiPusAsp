##  Contributors : 
##  Alice Julien-Laferriere
##  Delphine Parrot
##  
##  alice.julien.laferriere@gmail.com
##  delphine.parrot@gmail.com
##
##This software is the preprocessing step for MultiPus
#This software is governed by the CeCILL license under French law and
##abiding by the rules of distribution of free software.  You can  use, 
##modify and/ or redistribute the software under the terms of the CeCILL
##license as circulated by CEA, CNRS and INRIA at the following URL
##"http://www.cecill.info". 
##
##As a counterpart to the access to the source code and  rights to copy,
##modify and redistribute granted by the license, users are provided only
##with a limited warranty  and the software's author,  the holder of the
##economic rights,  and the successive licensors  have only  limited
##liability. 
##
##In this respect, the user's attention is drawn to the risks associated
##with loading,  using,  modifying and/or developing or reproducing the
##software by the user in light of its specific status of free software,
##that may mean  that it is complicated to manipulate,  and  that  also
##therefore means  that it is reserved for developers  and  experienced
##professionals having in-depth computer knowledge. Users are therefore
##encouraged to load and test the software's suitability as regards their
##requirements in conditions enabling the security of their systems and/or 
##data to be ensured and,  more generally, to use and operate it in the 
##same conditions as regards security. 
##
##The fact that you are presently reading this means that you have had
##knowledge of the CeCILL license and that you accept its terms.

def printTheNetworks( theNetworks):
    for i in theNetworks.iterkeys():
        print "======= network : " +  i  + "=========="
        (metabolites, reactions, index ) = theNetworks[i]
        for meta in index.iterkeys():
            print meta + " is number " + str( index[meta])
        for meta in metabolites.iterkeys():
            print metabolites[meta] 
        for reac in reactions.iterkeys():
            print reactions[reac]
        print " ====================== \n\n"


#########################        UTILS       ###########################################
## BEWARE, for both updae, theSubstrate/theProduct must be an object MyMetabolites (and not a list!)
'''def updateSubstrateReaction( theReaction, theSubstrate):
    theReaction.addSubstrate( [theSubstrate.getId()] )
    theSubstrate.addReactOut( [theReaction.getId ()] )

def updateProductReaction( theReaction, theProduct):
    theReaction.addProduct ( [theProduct.getId()  ] )
    theProduct.addReactIn  ( [theReaction.getId() ] )
'''
def readFileInList( filename ):
    f = open( filename, "r" )
    theList = [line.rstrip('\r\n') for line in f] # trailing character removed (either \n, \r\n or \r)
    f.close()
    return theList

######################" UTILS " ################################
# return boolean, true if edge, false if hyperedge (i.e: substrate or product > 1)
def isAnEdge (myReaction):
    return ( (len( myReaction.getSubstrates()) == 1) and (len( myReaction.getProducts()) == 1) )

def isTheSameR( reaction1, reaction2):
    sameSub =  ( set(reaction1.getSubstrates() ) == set(reaction2.getSubstrates()) )
    sameProd = ( set(reaction1.getProducts()   ) == set(reaction2.getProducts())   )
    return (sameSub and sameProd)