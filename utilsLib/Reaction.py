
## Alice Julien-Laferriere
## Delphine Parrot

class MyReaction :
# The attributes  available for all reactions are :
	## Id of the reaction (unique) an integer
	#__id
	## Id in the xml 
	#__idN
	## Name of the reaction (not necessary unique _from the xml is a string)
	#__name
	## substrate of the reactions, a list of ids of the substrates (see Metabolite class)
	#__substrates : SHOULD BE SETS
	## products, idem a list of id
	#__products : SHOULD BE SETS
	## specie of the reaction
	#__specie
	# __weight 
	# weight of the edge (reaction)/ will depend on whether it is a transition, an insertion or other stuff
## Constructor of the reaction, for having a reaction i need its id, its idName, and its Name
	def __init__(self, idInt, idN, name,  weight, subtrates = None, products = None):
		self.__id = idInt
		self.__name = name
		self.__idN = idN	
		self.__weight = weight
		if subtrates is None :
			self.__substrates = [] 
		else :
			self.__substrates = subtrates

		if products is None :
			self.__products = []
		else :
			self.__products = products
		


#=================================================#
#			Getter
#=================================================#
	def getId(self):
		return self.__id

	def getIdN(self):
		return self.__idN

	def getName(self):
		return self.__name

	def getSubstrates(self):
		return self.__substrates

	def getProducts(self):
		return self.__products
		
	def getWeight(self):
		return self.__weight



#=================================================#
#			Setter 
#=================================================#

# Updating name of the reaction in the middle. We can have information that we want to keep at the end such as insertion/transition ect.
	def addInName(self, stringToAdd):
	 #	print self.__idN
		tmpIdN = self.__idN.split("_")
		if(len( tmpIdN) > 1):
			self.__idN = "_".join( tmpIdN[0:-1] + [stringToAdd , tmpIdN[-1] ] ) 
		else:
			self.__idN = "_".join(  [tmpIdN[0]] + [stringToAdd] ) 

	def addSubstrate(self, idIntSubstrate):
		self.__substrates += idIntSubstrate
	def addProduct (self, idIntProduct):
		self.__products += idIntProduct
	def addMetab ( self, idIntSubstrate, idIntProduct):
		self.__substrates = idIntSubstrate
		self.__products = idIntProduct
	def removeSubstrate (self, idIntSubstrate):
		self.__substrates.remove(idIntSubstrate)
	def removeProduct   (self, idIntProduct):
		self.__products.remove(idIntProduct)
#=================================================#
#         Print                                   #
#=================================================#

	def __str__(self):
		stringToReturn = "Reaction Id : " + str( self.__id) + " has name: " + self.__name +  " and weight :" + str(self.__weight) + "\n"
		stringToReturn += "has substrate : "
		for subs in self.__substrates :
			stringToReturn +=  str(subs) + "  "
		stringToReturn += "\n and products : "
		for prod in self.__products : 
			stringToReturn += str(prod) + "  "

		return stringToReturn + "\n"

