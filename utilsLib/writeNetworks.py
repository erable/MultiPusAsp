##  Contributors : 
##  Alice Julien-Laferriere
##  Delphine Parrot
##  
##  alice.julien.laferriere@gmail.com
##  delphine.parrot@gmail.com
##
##This software is the preprocessing step for MultiPus
##This software is governed by the CeCILL license under French law and
##abiding by the rules of distribution of free software.  You can  use, 
##modify and/ or redistribute the software under the terms of the CeCILL
##license as circulated by CEA, CNRS and INRIA at the following URL
##"http://www.cecill.info". 
##
##As a counterpart to the access to the source code and  rights to copy,
##modify and redistribute granted by the license, users are provided only
##with a limited warranty  and the software's author,  the holder of the
##economic rights,  and the successive licensors  have only  limited
##liability. 
##
##In this respect, the user's attention is drawn to the risks associated
##with loading,  using,  modifying and/or developing or reproducing the
##software by the user in light of its specific status of free software,
##that may mean  that it is complicated to manipulate,  and  that  also
##therefore means  that it is reserved for developers  and  experienced
##professionals having in-depth computer knowledge. Users are therefore
##encouraged to load and test the software's suitability as regards their
##requirements in conditions enabling the security of their systems and/or 
##data to be ensured and,  more generally, to use and operate it in the 
##same conditions as regards security. 
##
##The fact that you are presently reading this means that you have had
##knowledge of the CeCILL license and that you accept its terms.

import sys  # for sys.exit : error
import json
from libsbml import *
from utils import isAnEdge
## Thiswill write a json (for dinghy fr example). It will help us to check some example and in the futur to print solutions
def writeNetworksJSON(prefixHyper, theNetwork  ):
    print "writing a json"
    linksDict = []
    nodesDict = []# array of dicts 
    counterNode = 0 
    countingMap = {}
    (metabolites, reactions, index ) = (theNetwork.getMetabolites(), theNetwork.getReactions(), theNetwork.getIndexMetabolites() )   
    subsystems = set()
        # Visit Metabolites
    for intId, myMetabolite in metabolites.iteritems():
        ## Filling the dictionnary
        newMeta = {"id" : myMetabolite.getId(), "shortname" : myMetabolite.getIdN(), "name": myMetabolite.getName(), "group": "metabolite"}
            # adding it to the nodes
        nodesDict += [newMeta]
        countingMap[myMetabolite.getId()] = counterNode
        counterNode += 1
    ## We do all metabolites in one row, to be able to keep the index of te metabolites...
    ## The reaction nodes are created aftwerward
    # Visit Reactions
    for intId, myReaction in reactions.iteritems():
        newReactNode = {"id": str(myReaction.getId() ), "name" : myReaction.getName(), "direction" : False, "orientation": ">", "group" : "reaction"}       
        nodesDict += [newReactNode]        
        for metabolite in myReaction.getSubstrates():
            newLink = {"source" : countingMap[metabolite], "target": counterNode, "orientation": ">"}# "name": myReaction.getIdN()
            linksDict += [newLink] ## one link by substrate   
        for metabolite in myReaction.getProducts():
            newLink = {"source" : counterNode, "target": countingMap[metabolite],  "orientation": ">"}#"name": myReaction.getIdN(),
            linksDict += [newLink] ## one link by product    
        counterNode=counterNode+ 1 # moving on to the next reaction
        
    with open( prefixHyper + ".json", "w") as jsonP:
        allData = { "version": [{"number" : "000"}], "nodes" : nodesDict,"links" : linksDict}
        json.dump(allData, jsonP, indent=2)

### TODO: this is really bad, fix just to see;.. TO do again WITH libSBML
def writeXML( name,  theNetwork ):
  (metabolites, reactions, index ) = (theNetwork.getMetabolites(), theNetwork.getReactions(), theNetwork.getIndexMetabolites() )    
## INSPIRED FROM http://sbml.org/Software/libSBML/docs/python-api/create_simple_model_8py-example.html
   # Create an empty SBMLDocument object.  It's a good idea to check for
   # possible errors.  Even when the parameter values are hardwired like
   # this, it is still possible for a failure to occur (e.g., if the
   # operating system runs out of memory).
  try:
    document = SBMLDocument(3, 1)
  except ValueError:
    raise SystemExit('Could not create SBMLDocumention object') 
   # Create the basic Model object inside the SBMLDocument object.  To
   # produce a model with complete units for the reaction rates, we need
   # to set the 'timeUnits' and 'extentUnits' attributes on Model.  We
   # set 'substanceUnits' too, for good measure, though it's not strictly
   # necessary here because we also set the units for invididual species
   # in their definitions.
  model = document.createModel()
  check(model,                              'create model')

  allSpecies = dict()
  for key, met in metabolites.iteritems():
        allSpecies[key] =  model.createSpecies()
        a = check(allSpecies[key],                                 'create species s1')
        a = check(allSpecies[key].setId(met.getIdN()),             'set species s1 id')
   # Create reactions inside this model, set the reactants and products,  
  allReactions = dict()
  for key, reac in reactions.iteritems():
        allReactions[key] = model.createReaction()
        a =check(allReactions[key],                                 'create reaction')
        a =check(allReactions[key].setId( str(reac.getIdN()) ),                     'set reaction id')
        a =check(allReactions[key].setReversible(False),            'set reaction reversibility flag')       
        for sub in reac.getSubstrates():
            species_ref1 = allReactions[key].createReactant()
            a =  check(species_ref1,                       'create reactant')
            a =  check(species_ref1.setSpecies( metabolites[sub].getIdN() ),      'assign reactant species')            
       
        for pro in reac.getProducts():  
          species_ref2 =  allReactions[key].createProduct()
          a =  check(species_ref2,                       'create product')
          #  print species_ref2.setSpecies(metabolites[pro].getIdN())
          a =  check(species_ref2.setSpecies(metabolites[pro].getIdN() ),      'assign product species')            
   # And we're done creating the  model.
   # Now write the model intro sbml format
  writeSBMLToFile(document, name)
#   return writeSBMLToString(document)



def check(value, message):
  """If 'value' is None, prints an error message constructed using
  'message' and then exits with status code 1.  If 'value' is an integer,
  it assumes it is a libSBML return status code.  If the code value is
  LIBSBML_OPERATION_SUCCESS, returns without further action; if it is not,
  prints an error message constructed using 'message' along with text from
  libSBML explaining the meaning of the code, and exits with status code 1.
  """
  if value == None:
    raise SystemExit('LibSBML returned a null value trying to ' + message + '.')
  elif type(value) is int:
    if value == LIBSBML_OPERATION_SUCCESS:
      return
    else:
      err_msg = 'Error encountered trying to ' + message + '.' \
                + 'LibSBML returned error code ' + str(value) + ': "' \
                + OperationReturnValue_toString(value).strip() + '"'
      raise SystemExit(err_msg)
  else:
    return


## Write the ASP file. BEWARE THE WEIGHTS BETWEEN XML AND ASP ARE NOT COMPARABLE YET
## IN ASP : new pseudo reactions = weight of 0.
## Otherwise the weight minimal is necessarly 1.
def writeASPinput( name, theNetwork, sources, targets ) : 
  if not ( len(sources) ):
      print "Your file is not ready to be treated directly by the C++ code....You need to add your sources"
  if not ( len(targets) ):
      print "Your file is not ready to be treated directly by the C++ code....You need to add your targets"
  print " writting networks"
  print "writing for theASP program in " + name + "_asp.txt"
  aspFile = open ( name + "_asp.txt", "w" )

  checkFileNode = open( name  + "_nodes.txt" ,  "w")
  checkFileEdge = open( name  + "_edges.txt" ,  "w")
 
  (metabolites, reactions, index) = (theNetwork.getMetabolites(), theNetwork.getReactions(), theNetwork.getIndexMetabolites() ) 
  for i , nia in metabolites.iteritems():
      checkFileNode.write( str(i ) + "\t" +  nia.getName() + "\n")
  checkFileNode.close()
  theLine = "%Declaration of all the hyperarcs  \n"
  theLine += "hyperarc("
  for reacId, reaction in reactions.iteritems():
      theLine += "\"" + reaction.getIdN() + "\"" + ";"
  aspFile.write( theLine[:-1] + ").\n")
  theLine = " %Declaration of the in and out of hyperarcs and their weights\n"
  aspFile.write( theLine)
  for reacId, reaction in reactions.iteritems():
    ## ASP 
    theLine = ""
    for sub in reaction.getSubstrates() : 
      theLine += "in(" +    str( sub ) + "," + "\"" +  reaction.getIdN() + "\"" + "). "
    for prod in reaction.getProducts() : 
      theLine += "out(" +  str( prod )  + "," + "\"" +  reaction.getIdN() + "\"" + "). "
    aspFile.write( theLine  + "\n")
    aspFile.write( "cost(" + "\"" +  reaction.getIdN() + "\""  + "," + str( reaction.getWeight() ) + ").\n" )
    ## EDGE FILE
    mySubstrates = map( lambda x : x.getIdN(), [ metabolites[k] for k in  reaction.getSubstrates()] )
    myProducts   = map( lambda x : x.getIdN(), [ metabolites[k] for k in  reaction.getProducts  ()] )
    edgeLine = reaction.getIdN() + "\t" +  ",".join( mySubstrates) + "\t" + ",".join( myProducts ) + "\t" + str( reaction.getWeight() ) + "\n"
    checkFileEdge.write( edgeLine )
  for s in sources :
    aspFile.write( "s(" + str( theNetwork.getMetIdWithName( s ) ) + ").\n")
  for p in targets : 
    aspFile.write( "t(" + str( theNetwork.getMetIdWithName( p ) ) + ").\n")
  aspFile.close()
  checkFileEdge.close()
