import json #writting with indentation 


# Will read the cofactor list in fact => return a list where each element is an n
def readFileInList( filename ):
    f = open( filename, "r" )
    theList = [line.rstrip('\r\n') for line in f] # trailing character removed (either \n, \r\n or \r)
    f.close()
    return theList





# Adapted From https://www.peterbe.com/plog/uniqifiers-benchmark
# Remove from a list (seq) duplicates 
def removeDuplicates(seq, idfun=None): 
   # order preserving
   if idfun is None:
       def idfun(x): return x
   seen = []
   result = []
   for item in seq:
       marker = idfun(item)
       if marker in seen : continue
       seen += [marker] 
       result.append(item)
   return result


## ATTENTION SUPER LONG SI ON EST PAS PROCHE DE L'optimum => ici on suppose que on a que les optimales ou presques!
## This is not satisfying, should get only the optimum inside
## NameFile : output of CLINGO
def readASPoutput ( nameFile) :
    print "reading " + nameFile
    uniqSolutions = dict() # index : answer number, value : the reactions
    solutions = []
    tt = open( nameFile, 'r' )
    scoreBef = float( "inf")
    for lineI in tt:
        line = lineI.rstrip('\r\n')
        if "Answer:" in line:
            nAnswer = int( line.split(" ")[-1] )
            solutionLine = tt.next( ).rstrip('\r\n')
            newLine = tt.next( ).rstrip('\r\n')          
            if not "Optimization" in newLine:
                print "ERRROR"
                raise SystemExit
            score = int( newLine.split(" ") [-1] )
            if score < scoreBef: # we had not reach the optimal 
                solutions = []
                scoreBef = score
            else :
                scoreBef = score
                print "BEST SCORE :  " + str( score ) 
            solution =  solutionLine.split(" ") 
            solution = [x for x in solution if not "go2source" in x ] # remove pseudo reactions from the solutions
            solution = [x for x in solution if not "go2target" in x ] # remove pseudo reactions from the solutions
            solutions +=  [solution ]
    tt.close()
    print "Number of Solutions : "  + str(len( solutions) )

    solutions = removeDuplicates ( solutions )
   
    for i in xrange(len( solutions )) : 
        uniqSolutions[i] =  solutions[ i ]
    print "Number of uniques Solutions : "  + str(len( uniqSolutions) )
    return uniqSolutions

# Reading the network as an edge file :
# one line : reacId, subs (sep: ",") , prods (sep: ","), weights)
def readTheEdges( nameFile ) : 
    ff = open( nameFile, "r")
    reactions = dict() # key :reaction Id, value = [[substrates] , [products ] ]
    for line in ff :
        ( reacId, subs, prods, ww )  = line.split( "\t")
        reactions[ reacId ]  =  [ subs.split(",") ,  prods.split(",") , int( ww.rstrip('\r\n') ) ]
    ff.close()
    return reactions

# ------------------------------------------------------------------------
#                           Writing JSON
# ------------------------------------------------------------------------
def writingTheJson( nameFile, metInSol, reactInSol, allCofactors, versionNumber ):
    countingmap={}  
    counter=0
    linksDict = []
    nodesDict = []# array of dicts 
    subsystems = set()
    species = set()
    counterReac = 0 # rmq : this will be our reaction name, TODO:  get them from the SBML
    for uniqId, met in metInSol.iteritems():
       # print uniqId, met
        newMet = {"id": met["idMet"], "shortname" :  met["idMet"], "name": met["name"], "group" : "metabolite", "compartment" :  met["compartiment"],  "cofactor" : int( met["idMet"] in allCofactors )}
        nodesDict += [newMet]
        countingmap[uniqId] = counter
        species.add( met["compartiment"]) # TODO : can be done before to avoid multiple check in add....
        counter += 1
    #print countingmap
    for react in reactInSol : 
        newReactNode = {"id" : counterReac, "name" : counterReac, "group" : "reaction", "value" : react[2], "subsystem": react[3] }
        subsystems.add (react[3])
        nodesDict += [newReactNode]
        for sub in react[0] :
            newLink = {"source" : countingmap[sub], "target": counter, "orientation": ">", "name" : counterReac, "value" : react[2], "subsystem" : react[3]}
            linksDict += [newLink]
        for prod in react[1] : 
            newLink = {"source" : counter, "target": countingmap[prod], "orientation": ">", "name" : counterReac, "value" : react[2], "subsystem" : react[3]}
            linksDict += [newLink]
        counter += 1 
    
    systemsDict = []
    for system in subsystems:
        color="#"
        color += hex((hash(system)% 160) +256 )[-2:]
        color += hex((hash(system)/160% 160) +256)[-2:]
        color += hex((hash(system)/160/160% 160) +256)[-2:]
        systemsDict +=  [{ "id": system, "color": color }]
    comparDict = []
    for speci in species:
        comparDict += [{ "id" : speci, "name " : speci}]

    print "writing in " + nameFile
    with open(nameFile , "w") as jsonP : 
        allData= {"version": [{"number" : versionNumber}], "nodes" : nodesDict,"links" : linksDict, "compartments" : comparDict,"systems" : systemsDict}
        json.dump(allData, jsonP, indent=2)

