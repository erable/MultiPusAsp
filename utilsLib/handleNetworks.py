## Alice Julien-Laferriere
## Delphine Parrot

import sys
from myNetwork import MyNetwork
from Reaction import MyReaction
from Metabolite import MyMetabolite
from SBMLread import readSBML



## In handle Networks : we need to => transform all networks
## filter the cofactors should be done while reading!
## Add free edges, 
## Add transition edges between metabolites of several networks with a weight of transition
## Do not insert source/sink reactions (without head nor tail) => DONE IN THE SBML READING ()
## Do not insert multiple edges => to check!
## Add reactions for the new network (insertion)
## Rmq : here network will be usually pass as a tupple of three dicts : metabolites, reactions , index (see readSBML)
## Sometimes the will be passe a dict themselves (idNets : thenetwork)
## We will keep track of the transform (splitted) metabolites in the dict call transforMets 


#For all network, split and transform them, while creating a big one....
def mergeAndTransformNetworks(allNetworks, sources, targets, weightTransition):
    transforMetsByNet = dict()
    #newNetwork = MyNetwork() ## RMQ: we keep the network before transformation to be able to reprint it (for ex in json in Dinghy)
    ## Checking
    newMergeNetwork = MyNetwork( "merged")
    for i in allNetworks.iterkeys() :        
        transforMets = newMergeNetwork.mergeNetwork( allNetworks[i] )
       
        transforMetsByNet[i] = transforMets
    
    transitions(transforMetsByNet, newMergeNetwork, weightTransition)
  #  newMergeNetworks[1].update ( transitionsReact )

    addFinalSources(newMergeNetwork,  transforMetsByNet, sources)
    addFinalTargets(newMergeNetwork,  transforMetsByNet, targets)
    return (newMergeNetwork, transforMetsByNet)



#networksMet is a dict of dict
# first key : species
# second key : metabolite
# value of the second dict: idMetIn, idMetOut
def transitions( networksMet, newNetwork, weightTransition):
    ## take all networks two by two
    theNetworks = networksMet.keys()
    for i in range(0, len(theNetworks)):
        for j in range(i+1, len(theNetworks)):
            net1 = networksMet[ theNetworks[i] ] # dict : old metabolite name, new metabolite name
            net2 = networksMet[ theNetworks[j] ]
            for originalMet in net1.keys():
                if originalMet in net2.keys(): #also in network 2 => do the transition                                 
                    net1Name =  theNetworks[i]
                    net2Name = theNetworks[j]
                    ### 1 TO 2      
                    nameR = "TransitionOf_"+ originalMet + "_" + net1Name + "to" + net2Name
                    reac1to2 = MyReaction( newNetwork.getNextIdR(), nameR, nameR, weightTransition, [ newNetwork.getMetIdWithName( net1[originalMet]) ], [newNetwork.getMetIdWithName(net2[originalMet]) ] )# to do transition weight as parameter
                    newNetwork.addReaction( reac1to2 )  
    
                    nameR = "TransitionOf_"+ originalMet + "_" + net2Name + "to" + net1Name
                    reac2to1 = MyReaction( newNetwork.getNextIdR(), nameR, nameR, weightTransition, [ newNetwork.getMetIdWithName(net2 [originalMet]) ], [newNetwork.getMetIdWithName( net1[originalMet]) ] ) # to do transition weight as parameter   
                    newNetwork.addReaction( reac2to1 )
                


#Reaction 1, 2 : object reaction to compare
# Metabolites1, 2 : map metabolites {int, Metabolite object}
def isTheSameReaction( reaction1, reaction2, metabolites1, metabolites2 ):
    isTheSame = True
    ## Checking the substrates: must be the same
    allSubstrates1 = [ metabolites1[k] for k in reaction1.getSubstrates()] # extract a list of metabolites, the su
    allSubstrates2 = [ metabolites2[k] for k in reaction2.getSubstrates()]
    if  len(allSubstrates2) != len(allSubstrates1) :
        isTheSame = False
    else :# is it really the same?
        namesSub1 = map( lambda x : x.getIdN(), allSubstrates1 )
        namesSub2 = map( lambda x : x.getIdN(), allSubstrates2 )
        if len( set(namesSub2) ^ set(namesSub1) ) != 0 :  # symtric fferences of 2 sets => 
            isTheSame = False
    # if substrate are the same, we need to check the products...
    if isTheSame :
        allProducts1 = [ metabolites1[k] for k in reaction1.getProducts()]
        allProducts2 = [ metabolites2[k] for k in reaction2.getProducts()]
        if  len(allProducts2) != len(allProducts1) :
            isTheSame = False
        else :# is it really the same?
            namesPro1 = map( lambda x : x.getIdN(), allProducts1 )
            namesPro2 = map( lambda x : x.getIdN(), allProducts2 )        
 
            if len( set(namesPro2) ^ set(namesPro1) ) != 0 :  # we have to sort in case they were not inserted in the same order
                isTheSame = False
    
    return isTheSame


##### We prohibit multiple edges. To do so we need to read the network, check for all the reactions whether the already exist....if not, insert....
## If a metabolite is not in the "host" network
## For the moment we do not distinguish the different network.
## If we want to do so, we will need to insert new reaction only if weight lower than the previous one inserted...
def insertionNetwork( fileName, myNetworks, cofactorsList, weightInsert ):
    iNetwork = readSBML( fileName, cofactorsList, 0.0, "insert" )
            ##once we have the metabolite we can check for the reactions needed
            #check if a reaction to insert in all the reactions  already there....
    count = 0
    countAdd = 0
    print "network I has " + str(iNetwork.getNextIdR() ) + " reactions "
    for i in myNetworks.iterkeys(): # key : name network, value = (metabolites, reactions, index)
        print " Insertion for network : " + i
        currentNet = myNetworks[i]
        ## NB : i do not need togo through the metabolites, we just need to go through the reaction
        # For all reaction in insertion organism, check it is in the host
        for reactKeyI, currentReaction in iNetwork.getReactions().iteritems():
            doNotHave = True
            for reactKeyH, reactHost in currentNet.getReactions().iteritems():
                if isTheSameReaction( reactHost, currentReaction, currentNet.getMetabolites(), iNetwork.getMetabolites()):
                    count += 1 
                    doNotHave = False
                    break
                    ## If I doNotHave the reaction, need to insert from I to H
            if doNotHave:
                    myNetworks[i].insertReaction( iNetwork, currentReaction, weightInsert )  ## def insertReaction(networkH, networkI, reactr):
                    ## else : nothingToDo
                    countAdd +=1 
                ## DO THE SAME FOR IN REACTION 
    print  str(count) + " reactions were already there"
    print  str(countAdd) + " reactions were inserted with weight " + str(weightInsert)



## newMergeNetwork is  the full netwok
## transorMetByNet is a dict of dict. 
## First dict as key : the origin network, and second dict has 
## key : origianl name (before transfo), value: new name () (attached with the netwrok)
## name = IdN
def addFinalSources(newMergeNetwork,  transforMetsByNet, sources):
    
    counterSource = { s : 0 for s in sources}
    for s in sources:
        print "adding mega source " + s
                ## RMQ : do not change the IDN of the final Sources and targets without modifying writeNetworks !
        superSource = MyMetabolite( newMergeNetwork.getNextIdM(), s + "_s", s + "_s")
        newMergeNetwork.addMetabolite( superSource )
        for idNet, metMap in transforMetsByNet.iteritems():
            if s in metMap.keys():
                theReaction = MyReaction( newMergeNetwork.getNextIdR() , "From" + idNet + "_go2source_" + s, "go2source_" + s, 0)
                theReaction.addSubstrate( [ superSource.getId() ] )
                theReaction.addProduct (  [ newMergeNetwork.getMetIdWithName(metMap[s]) ] )
                newMergeNetwork.addReaction( theReaction ) # will update the substrate/products adjacency list

                counterSource[ s ] += 1
    for s, value in counterSource.iteritems():
        print      "Source : " + s  + " was initially in " +str(value) + " network"
        

def addFinalTargets(newMergeNetwork,  transforMetsByNet, targets):
    
    counterTarget = { t : 0 for t in targets}
    for t in targets:
        print "adding mega target " + t
        ## RMQ : do not change the IDN of the final Sources and targets without modifying writeNetworks and rainbows ==> TODO :utils FUntion! !
        superTarget = MyMetabolite( newMergeNetwork.getNextIdM(), t + "_t", t + "_t")
        newMergeNetwork.addMetabolite( superTarget )
        for idNet, metMap in transforMetsByNet.iteritems():
            ## RMQ : here if the metabolite is not present at all in the networks, network not linked to the end targets...
            if t in metMap.keys():
                theReaction = MyReaction( newMergeNetwork.getNextIdR(), "From" + idNet + "_go2target_" + t, "go2target_" + t, 0)              
                theReaction.addSubstrate( [newMergeNetwork.getMetIdWithName (metMap[t]) ] )
                theReaction.addProduct( [ superTarget.getId() ] )
                newMergeNetwork.addReaction( theReaction )
                counterTarget[t] += 1

        ## check that the target is at least reachable through one network
    for t, value in counterTarget.iteritems():
        if value == 0:
            sys.exit( "Target : " + t  + " not found in all the networks EXITING" )
        else : 
            print      "Target : " + t  + " was initially in " + str(value) + " network"
