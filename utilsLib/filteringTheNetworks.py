##  Contributors : 
##  Alice Julien-Laferriere
##  Delphine Parrot
##  
##  alice.julien.laferriere@gmail.com
##  delphine.parrot@gmail.com
##
##This software is the preprocessing step for MultiPus
#This software is governed by the CeCILL license under French law and
##abiding by the rules of distribution of free software.  You can  use, 
##modify and/ or redistribute the software under the terms of the CeCILL
##license as circulated by CEA, CNRS and INRIA at the following URL
##"http://www.cecill.info". 
##
##As a counterpart to the access to the source code and  rights to copy,
##modify and redistribute granted by the license, users are provided only
##with a limited warranty  and the software's author,  the holder of the
##economic rights,  and the successive licensors  have only  limited
##liability. 
##
##In this respect, the user's attention is drawn to the risks associated
##with loading,  using,  modifying and/or developing or reproducing the
##software by the user in light of its specific status of free software,
##that may mean  that it is complicated to manipulate,  and  that  also
##therefore means  that it is reserved for developers  and  experienced
##professionals having in-depth computer knowledge. Users are therefore
##encouraged to load and test the software's suitability as regards their
##requirements in conditions enabling the security of their systems and/or 
##data to be ensured and,  more generally, to use and operate it in the 
##same conditions as regards security. 
##
##The fact that you are presently reading this means that you have had
##knowledge of the CeCILL license and that you accept its terms.


import sys


from Reaction import MyReaction
from Metabolite import MyMetabolite
from writeNetworks import writeXML
## Rmq: this could be done in filtering the network. Separated to help debugging
## If we have an hyperarcs of badness 0 (nb of head nodes = 1 , only several tail nodes), then we have to split it.
## Eg with a hyperarcs (a) ---w--> (b,c) of weight w
## creation of Mprime Met and  new reactions replace the previous one as:
## (a) --w--> Mprime, Mprime -- 0.0 --> b, Mprime --0.0 --> c

def filteringTheNetwork (theNetwork, newSources,  newTargets, nbNetworks):
    continueF = True
    counterMetabolites = 0
    counterReactions = 0
    count = 0
    while continueF :  
        counterReactions = 0
        removedReaction = removeSinkUptakeReactions(theNetwork)
        removedMetabolite = filterDeg1Nodes( theNetwork, newSources,  newTargets,  counterReactions, nbNetworks )
        continueF = (removedReaction>0) or (removedMetabolite>0)
        counterMetabolites += removedMetabolite
        if continueF:
            print  str(removedMetabolite) + " removed Metabolites"
            print  str(removedReaction + counterReactions) + " removed Reactions"
        
        count+=1
       # print " continueF is " + str(continueF) + ": " + str(removedReaction>0) +  " OR " + str(removedMetabolite>0)
    print "we passed in the filters " + str(count) + " times"


## Removes the reaction without substrate or products
def removeSinkUptakeReactions( theNetwork ):
   # print "in removeSinkUptakeR"
    toDeleteR = []
    for idR, reac in theNetwork.getReactions().iteritems():
        if ( len( reac.getSubstrates()) == 0 )  or ( len( reac.getProducts() ) == 0 ): # no substrates
            toDeleteR += [ reac.getId() ]

    for idR in toDeleteR:
        theNetwork.removeReaction(idR)    
    #print str(len(toDeleteR)) +  " reactions were removed because without substrates OR products"
    #print toDeleteR
    return (len(toDeleteR))


## Removes nodes that are not sources nor targets
def filterDeg1Nodes( theNetwork, sources, targets,  counterReactions, nbNetworks ):
    #print "in filterDeg1Nodes"
    counterTarget = { key : 0 for key in targets} #we should stop if a target is never reachable
    notFinished = True
    counterRemovalMet = 0
    while notFinished :        
        notFinished = False
        toRemove = []
        for (idM , metabolite) in theNetwork.getMetabolites().iteritems():
        ## Is the metabolite a sink?     
            if metabolite.getDegOut() == 0:
                if metabolite.getIdN() not in targets:
                   toRemove += [idM]
                if metabolite.getIdN() in sources :
                    print " Source " + metabolite.getIdN() + " is not usable in network " 
        ## Is the metabolite a source :
            elif metabolite.getDegIn() == 0:
                if metabolite.getIdN() not in sources:
                    toRemove += [idM]
                if metabolite.getIdN() in targets:
                    print  "Target " + metabolite.getIdN() + " is not reachable in network "
                    counterTarget [ metabolite.getIdN() ] += 1
        for tt in toRemove :
            theNetwork.removeMetaboliteByID( tt )
        counterRemovalMet   += len(toRemove)        
        notFinished = ( len( toRemove ) > 0 )

    for (idM, counter) in counterTarget.iteritems():
        if counter == nbNetworks:
            sys.exit( "Target " + str(idM) + " is never reachable")    
    #print str( counterRemovalMet )  + " metabolites were removed because sink or sources"
    #print str( counterReactions )  + " reactions were removed as consequences"
    return counterRemovalMet


def toDisconnectNodes(theNetwork, visited,  sources, targets):
   # nbRemoved = len(visited) - sum(visited)
    toto = []
    removedS = []
   # print "nb disconned nodes removed : " + str(nbRemoved)
    for indexI, status in visited.iteritems():
        if not status:
            metaboliteName= theNetwork.getMetabolite(idM = indexI).getIdN()
            if metaboliteName in targets:
                writeXML("errorDisconnect_" + metaboliteName + ".xml", theNetwork)
                sys.exit(" Disconnecting the target " + metaboliteName)
            if metaboliteName in sources:
                print "removing source : " + metaboliteName
                removedS += [metaboliteName]
            toto += [ metaboliteName ]
            theNetwork.removeMetaboliteByID( indexI )
    '''if len(toto):
        print "removed  " 
        print toto
        print " new network size ; " + str(theNetwork.getNbMetabolites() ) +  " , " + str(theNetwork.getNbReactions() )
    
    else :
        print "no met to remove"
    '''
    return removedS 
   # print "===================="
   


## Check if the nodes are connected to sources or target, otherwise to remove
## RMQ: this might be done way more efficiently
## Here we alway start from a source, with dfs, every node reach is visited.
## At the end , all non visited nodes should be removed.
def disconnectedGraph(theNetwork, sources, targets):
    print "in disconnectedGraph !"
    #visited =  [False] * theNetwork.getNbMetabolites( )  ## initialisation   
    visited = dict() # key is id Metabolite, value is wheter visited or not
    ############### Disconnecting because of the sources
    for i in theNetwork.getMetabolites().iterkeys():
        visited[i] = False
    ## Launching for all sources :

    for s in sources:
        metS = theNetwork.getMetabolite( name = s)
        visited[  metS.getId()  ] = True
        dfs( theNetwork, metS, visited )
    print "calling toDisconnect from sources: "
    removedS = toDisconnectNodes( theNetwork, visited, sources, targets)
    # end disconnecting sources
    ############### Disconnecting because of the targets not reachable
    visited2 =  dict() # key is id Metabolite, value is False/True, whether visited or not
    for i in theNetwork.getMetabolites().iterkeys():
        visited2[i] = False
    counter = 0
    for t in targets: 
        metT = theNetwork.getMetabolite( name = t)
        visited2[  metT.getId() ] = True
        dfs_reverse( theNetwork, metT, visited2)
    print "calling toDisconnect from targets: "
    removedS += toDisconnectNodes( theNetwork, visited2, sources, targets)
    for s in removedS:
        sources.remove(s)
        print "removing " + s + " from the sources"

    ### Now all visited == False means that they are disconnected from the sources and targets
    ## I will never reached them nor use them to reach the products
    
# u is a myMetabolite object
def dfs(theNetwork, u, visited):
    allReactionOut = u.getReactOut()
    for react in allReactionOut:
        reachedNodes = [ theNetwork.getMetabolite( idM = k) for k in theNetwork.getReaction( react ).getProducts() ]
       # print reachedNodes
        for toto in reachedNodes:
            if not visited[ toto.getId() ]:
                visited[ toto.getId() ] = True
                dfs( theNetwork, toto, visited)


# u is a myMetabolite object
def dfs_reverse(theNetwork, u, visited):
    allReactionIn= u.getReactIn()
    for react in allReactionIn:
        reachedNodes = [ theNetwork.getMetabolite( idM = k) for k in theNetwork.getReaction( react ).getSubstrates() ]
       # print reachedNodes
        for toto in reachedNodes:
            if not visited[  toto.getId() ]:
                visited[ toto.getId() ] = True
                dfs_reverse( theNetwork, toto, visited)