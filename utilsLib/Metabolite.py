
## Alice Julien-Laferriere
## Delphine Parrot

class MyMetabolite :
# The attributes  available for all metabolites are :
	## Id of the metabolite (unique) an integer
	#__id
	## Id in the xml 
	#__idN
	## Name of the metabolite (not necessary unique _from the xml is a string)
	#__name
	## Incoming reactions (list)
	#__reactIn
	## Outcoming reactions (list)
	#__reactOut
	
## Constructor of the metabolite, for having a metabolite i need its id, its idName, and its Name
	def __init__(self, idInt, idN, name):
		self.__id = idInt
		self.__name = name
		self.__idN = idN
		self.__reactIn = []
		self.__reactOut = []

#=================================================#
#			Getter                                #
#=================================================#
	def getId(self):
		return self.__id

	def getIdN(self):
		return self.__idN

	def getName(self):
		return self.__name
	
	def getReactIn(self):
		return self.__reactIn

	def getReactOut(self):
		return self.__reactOut

	def getDegIn(self):
		return len( self.__reactIn  )

	def getDegOut(self):
		return len( self.__reactOut )

#=================================================#
#			Setter                                #
#=================================================#
# Beware: idReac  must be a list
	def addReactIn  (self, idReac): 
		self.__reactIn  += idReac
	def addReactOut (self, idReac):
		self.__reactOut += idReac

	# Beware: idReac  must be a list
	def removeReactIn (self, idReac): 
		self.__reactIn.remove ( idReac )
	def removeReactOut (self, idReac):
		self.__reactOut.remove( idReac )
#=================================================#
#		Methods									  #
#=================================================#




#How to split a metabolites; will create two, the one in and the one out..
# To only call in handleNetworks (will  add transition reaction )
# reactions will be handle there also
	def splitMetabolite(self, startId, idNetwork):
		# division in two:
		metaInId =   idNetwork + "_" + self.getIdN() + "_in"
		metaIn = MyMetabolite(startId, metaInId, metaInId) #here we do not take into account the name...same as IdN. TODO : remove name after a while
		
		metaOutId =  idNetwork + "_" + self.getIdN() + "_out"
		metaOut = MyMetabolite( startId + 1, metaOutId, metaOutId) #here we do not take into account the name...same as IdN. TODO : remove name after a while
		
		
		return (metaIn, metaOut)


#=================================================#
#         Print                                   #
#=================================================#

	def __str__(self):
		stringToReturn = "Metabolite Id : " + str( self.__id) + " has name id: " + self.__idN + "\n"
		stringToReturn += "has reactIn : "
		for reactIn in self.__reactIn :
			stringToReturn +=  str(reactIn) + "  "
		stringToReturn += "\n and reactOut : "
		for reactOut in self.__reactOut : 
			stringToReturn += str(reactOut) + "  "

		return stringToReturn + "\n"

