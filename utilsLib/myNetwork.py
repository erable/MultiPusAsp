
## Alice Julien-Laferriere
## Delphine Parrot

from Metabolite import *
from Reaction import *

from utils import isAnEdge
from copy import deepcopy

class MyNetwork :
    ## the network contains all metabolites, reactions 

    def __init__(self, name):
        
        self.__name = name
        self.__metabolites = dict() # key : id, value : MyMetabolit object
        self.__indexMetabolites = dict()
        self.__theReactions = dict()
        


#=================================================#
#           Getter
#=================================================#
    def getName(self):
        return self.__name

    def getNbMetabolites(self):
        return len( self.__metabolites)

    def getNbReactions(self):
        return len( self.__theReactions)

    def getMetabolites(self):
        return self.__metabolites

    def getIndexMetabolites(self):
        return self.__indexMetabolites
        
    def getReactions(self):
        return self.__theReactions


    def getMetabolite(self, name=None, idM= None):
        if not name is None:
            return self.__metabolites[ self.__indexMetabolites[ name ]]
        if not idM is None:
            return self.__metabolites[idM]
        else :
            print "error in GetMetabolite, no args given"

    def getMetNameWithId( self, idM):
        return self.__metabolites[idM].getIdN()
    
    def getMetIdWithName( self, name):
        return self.__indexMetabolites[name]


    def getReaction(self, idR):
        return self.__theReactions[ idR ]


    def getNextIdR ( self):
        if len (self.__theReactions) > 0 :
            return (max(self.__theReactions.keys()) + 1)
        else : 
            return 0



    def getNextIdM ( self ):
        if len(self.__metabolites) > 0:
            return (max(self.__metabolites.keys()) + 1)
        else :
            return 0
        
    #metabolite MUST BE AN object MyMetabolte
    def hasMetabolite(self, metabolite):
        return metabolite.getIdN() in self.__indexMetabolites.keys()

#=================================================#
#           Setter 
#=================================================#

    def addMetabolite(self, metabolite):
        ## TODO : update reaction? => no
        ## DEBUG
        '''
        if metabolite.getId() in self.__metabolites.keys():
            print "metabolites ID already present"
            print self.__metabolites[metabolite.getId()]
            exit(0)
        '''
        ## end debug
        self.__metabolites[ metabolite.getId() ] = metabolite
        self.__indexMetabolites[ metabolite.getIdN() ] = metabolite.getId()
                
    ## Will update the reactions around the metabolites, removing them, or updating tail of the hyperarcs.
    ## Do not remove the metabolite
    ## if deg in = 0  (only outgoing edges) and not a sources:
    # ==>  we can remove all the out-edges and the metabolites since we will never be able to product it therefore taking the edge
    ## if deg out = 0  (only ingoing edges) and not a target:
    # ==> we can remove the metabolite and keep the hyper-edges (but updating the product list to remove it)
    def checkAndPrepareRemoval(self, metabolite):
        toDel = []
        allReactionsOut =  deepcopy(metabolite.getReactOut()) ## BEWARE: need deepcopy to iterate over AND remove from metaoblite adjacency list
        for reackey in allReactionsOut : 
            metabolite.removeReactOut( reackey )  
            toDel += [ reackey]
        allReactionsIn  =  deepcopy(metabolite.getReactIn () )
        currentReactionIn = {thekey : self.getReaction(thekey) for thekey in allReactionsIn}
        for reackey, reaction in currentReactionIn.iteritems() : 
            metabolite.removeReactIn( reackey )    
            if isAnEdge( reaction ):
                toDel += [reackey]          
            else: # we need to remove the metabolite and clean the reaction (removing metabolite  as a product) but not remove it
                self.updatingTailHyperArcs ( metabolite, reackey )
        for dd in set(toDel) : ## to avoid cases where we have reaction such as A -> A (or more complex : A +B -> A+ C)
            self.removeReaction( dd )
        #return count # return number of removed reaction


    def removeMetaboliteByID(self, metaboliteID):
        metabolite = self.__metabolites[ metaboliteID ]
        self.removeMetabolite( metabolite )
    

## Will only remove the metabolites! not the reactions attached
    def removeMetabolite(self, metabolite):    
        self.checkAndPrepareRemoval( metabolite )
        del self.__metabolites[ metabolite.getId() ] 
        #print self.__indexMetabolites
        del self.__indexMetabolites[ metabolite.getIdN() ] 
        

    def addReaction(self, reaction, preNet = ""): # prenet is if a prefixe for the networks is needed        
### DEBUG 
        '''
        if reaction.getId() in self.__theReactions.keys():
            print "reaction ID already present"
            print self.__theReactions[reaction.getId()]
            exit(0)
        '''
### DEBUG  

        self.__theReactions[ reaction.getId() ] = reaction
        for sub in reaction.getSubstrates():
                if reaction.getId() not in self.__metabolites[sub].getReactOut():
                    self.__metabolites[sub].addReactOut( [reaction.getId()] )
        for prod in reaction.getProducts():
                if reaction.getId() not in self.__metabolites[prod].getReactIn():
                    self.__metabolites[prod].addReactIn( [reaction.getId()] )


    def removeReaction(self, reactionId):
        reaction = self.__theReactions[reactionId]
        for sub in reaction.getSubstrates():
            if reactionId in self.__metabolites[sub].getReactOut():
                self.__metabolites[sub].removeReactOut( reaction.getId() )
        for prod in reaction.getProducts():
            if reactionId in self.__metabolites[prod].getReactIn():
                self.__metabolites[prod].removeReactIn( reaction.getId() )      
        del self.__theReactions[reactionId]

## Insert the reaction : reactr (from Insertion Networks) into the Host Network
## I is for insertion, H is for HOST
# will update the metabolites if needed
    def insertReaction(self, networkI, reactr, weightInsert ):    
        myNewReaction = MyReaction(self.getNextIdR() , reactr.getIdN() + "_inserted", reactr.getName() + "_inserted", weightInsert ) 
        for subsId in reactr.getSubstrates():
            metaI = networkI.getMetabolite( idM = subsId)        
        #    print metaI
            ## If metabolite not there we need to create it
            if not self.hasMetabolite( metaI ) : 
                newMetabolite = MyMetabolite( self.getNextIdM(), metaI.getIdN(), metaI.getName() )
                self.addMetabolite( newMetabolite  )
                myNewReaction.addSubstrate( [newMetabolite.getId()] )  
            else :
                myNewReaction.addSubstrate( [ self.getMetIdWithName( metaI.getIdN() ) ] )      
        for prodId in reactr.getProducts():
            metaI  = networkI.getMetabolite( idM = prodId )
         #   print metaI     
            if not self.hasMetabolite( metaI ) : #this metabolite does not exist in the host network
                newMetabolite = MyMetabolite ( self.getNextIdM(), metaI.getIdN(), metaI.getName() )
                self.addMetabolite( newMetabolite  )
                myNewReaction.addProduct( [newMetabolite.getId() ])
            else :
                myNewReaction.addProduct( [self.getMetIdWithName( metaI.getIdN() ) ] )     
       # print myNewReaction
        self.addReaction( myNewReaction)

    ## Adding another network to this one.
    ## Will just reindex all metabolites and add the reaction.
    ## Will not take care of adding some transition reaction
    ## Return a dict to get the correspondence between the id in the othernetwork and the id in this network
    def mergeNetwork(self, otherNetwork):
        oldNewMet = dict() #key : oldId (in otherNetwork), value : newId (in self)
        networkName = otherNetwork.getName()
        ## since no split easier
        ## UpdateMetabolite :
        for idM, metabolite in otherNetwork.getMetabolites().iteritems():
            newMet = MyMetabolite( self.getNextIdM(), networkName + "_" + metabolite.getIdN(),  networkName + "_" + metabolite.getIdN())
            oldNewMet[ metabolite.getIdN() ] = newMet.getIdN ()
            self.addMetabolite( newMet )

        for idR, reaction in otherNetwork.getReactions().iteritems():
            newReac = MyReaction( self.getNextIdR(), networkName + "_" + reaction.getIdN(), reaction.getName(), reaction.getWeight() )          
            for oldSub in reaction.getSubstrates():
                newReac.addSubstrate( [ self.getMetIdWithName( oldNewMet[ otherNetwork.getMetNameWithId(oldSub) ]) ] )
            for oldProd in reaction.getProducts():
                newReac.addProduct( [self.getMetIdWithName( oldNewMet[otherNetwork.getMetNameWithId( oldProd )  ])] ) 
            self.addReaction( newReac, networkName )
        return oldNewMet


## This method is called when we want to remove a vertex/metabolite part of tail of an hyperacs.
# Takes in parameters, the metabolite to remove, the current edge being seen
# The reaction name will be updatate to remember that we had another product, eventhough we do not need him in the second part of the algorthim
    def updatingTailHyperArcs(self, metabolite, reactionToUpdateID):
        self.__theReactions[reactionToUpdateID].removeProduct( metabolite.getId() )
        oldName = metabolite.getIdN()
        nn = ["WithProd"] + oldName.split("_")[1:-1] # get only the initial Id 
        self.__theReactions[reactionToUpdateID].addInName( "_".join(nn) )


#=================================================#
#         Print                                   #
#=================================================#

    def __str__(self):

        stringToReturn = self.__name + "NB Reaction : " + str( len( self.__theReactions))  +  " and Metabolites : " + str(len( self.__metabolites )) 
       # for i, reac in self.__theReactions.iteritems():
       #     print reac
       # for i, met in self.__metabolites.iteritems():
       #     print met
        return stringToReturn + "\n"

