
To execute Multipus, three commands are required.

clingo (>= 4.5*) is needed.

First a preprocessing is done.

Here we have to distinguish two type of sbml file 
- the working organisms
- the insertiong organisms
if several organisms are used, put the SBML between quotes, separated by simple space

======= Preprocessing ===========
Is a python Script allowing to preprocess the networks

Example :

 ./asp_preprocessing "XML/workingOrganism1.xml XML/workingOrganism2"  --idsNet "WO1 WO2" --source XML/sources.txt --tar XML/targets.txt --co XML/cofactors.txt --inser "XML/InsertionOrganism.xml" --out "OutDirectory"


======= Getting Solutions  =========== 
##pathToCLingo, tested with Clingo 4.5.*
# If you know a maximal bound (e.g: 528 ) 
pathToClingo/clingo -t 4 --opt-mode=optN --opt-bound=528   OutDirectory/finalNet_asp.txt Rainbow.lp > outputClingo.OU 
# Otherwise just use
pathToClingo/clingo -t 4    OutDirectory/finalNet_asp.txt Rainbow.lp > outputClingo.OU 

( the option --opt-mode=optN can also be used to enumerate)



======= see the solutions  ===========

Once the solutions are obtained, it is possible to extract a more clear vu using the
toDinghyJson_fromClingo.py script.

./toDinghyJson_fromClingo.py  --co XML/cofactor.txt outputClingo.OU  OutDirectory/finalNet_edges.txt  "XML/workingOrganism1.xml XML/workingOrganism2 XML/InsertionOrganism.xml"

