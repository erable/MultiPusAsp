#!/usr/bin/env python
##  Contributors : 
##  Alice Julien-Laferriere
## alice.julien.laferriere@gmail.com
##
##This software is a computer program whose purpose is to extract an json file from 
## a  Multipus Solutions obtained with Clingo and sbml files in order to visualize it afterward.
#This software is governed by the CeCILL license under French law and
##abiding by the rules of distribution of free software.  You can  use, 
##modify and/ or redistribute the software under the terms of the CeCILL
##license as circulated by CEA, CNRS and INRIA at the following URL
##"http://www.cecill.info". 
##
##As a counterpart to the access to the source code and  rights to copy,
##modify and redistribute granted by the license, users are provided only
##with a limited warranty  and the software's author,  the holder of the
##economic rights,  and the successive licensors  have only  limited
##liability. 
##
##In this respect, the user's attention is drawn to the risks associated
##with loading,  using,  modifying and/or developing or reproducing the
##software by the user in light of its specific status of free software,
##that may mean  that it is complicated to manipulate,  and  that  also
##therefore means  that it is reserved for developers  and  experienced
##professionals having in-depth computer knowledge. Users are therefore
##encouraged to load and test the software's suitability as regards their
##requirements in conditions enabling the security of their systems and/or 
##data to be ensured and,  more generally, to use and operate it in the 
##same conditions as regards security. 
##
##The fact that you are presently reading this means that you have had
##knowledge of the CeCILL license and that you accept its terms.



from copy import deepcopy ## needed for deepd copy
import argparse #python Licence
from libsbml import *

from utilsLib.utilsSolution import *


# SameReactions checks in possibleReactions if one contains all substrates and products needed and return the one found
# Rmq: reactionsList come from the reading of the sbml, hence contains ALL substrates and products (including cofactors)
# subs and prods, contrains the one givent by the software, hence trimmed of all coffactors
def sameReaction ( possibleReactions, subs, prods, reactionsList):
    for reacId in possibleReactions:
            isTheSame = True
            reaction = reactionsList[reacId]
            for s in subs : 
                if s["idMet"] not in reaction[0]:
                    isTheSame = False
                    break
            for p in prods:
                if p["idMet"] not in reaction[1]:
                    isTheSame = False
                    break
            if isTheSame :  
                break
    if not isTheSame:
        reacId="notFound"
    return (isTheSame, reacId)


def fillSub ( subOfReaction, substratesList, idReaction):
    for i in substratesList:
        if i  not in subOfReaction:
            subOfReaction[i] = [] # creating the empty list
        subOfReaction[i] += [idReaction]



## Reading the SBML
# currentMap : dict : id , name 
# reactionList : a map containing [idReactin] = [substrats], [products]
# subOfReacrion : a map connainng [ idSubstrat] = [list of reactions in which the compound is a substrate]

def getIdN( fileName, currentMap, reactionsList, subOfReaction):
    print "opening : " + fileName    
    reader = SBMLReader()
    mySBMLDoc = reader.readSBML(fileName)   
    errors = mySBMLDoc.getNumErrors() # not necessary  
    if( errors > 0):
        print(" validation error(s): " + str(errors))
    network = mySBMLDoc.getModel()
    allSpecies = network.getListOfSpecies()
    for specie in allSpecies: #recherche dans la section species
        currentMap[specie.getId()] = specie.getName()
    ## If I need the cofactors
    allReactions = network.getListOfReactions()
    for reaction in allReactions:
            idR = reaction.getId();
            idSubstrats = []
            idProducts = []
            for sub in reaction.getListOfReactants():
                idSubstrats += [sub.getSpecies() ]
            for prod in reaction.getListOfProducts():
                idProducts += [prod.getSpecies() ]
            reactionsList[idR] = [ idSubstrats, idProducts ]
            fillSub( subOfReaction, idSubstrats, idR)
            if reaction.getReversible():
                 reactionsList[idR + '_rev'] = [ idProducts, idSubstrats ]
                 fillSub( subOfReaction, idProducts, idR + '_rev')

#nameMetSol : name in the solution : format : Espece_MetId
def getInfo(idMet, mapOfMet):
    splitMet = idMet.split("_") #remove specie name 
    uniqueId=""
    if splitMet[-1]=="s":
        idMet = "_".join(splitMet[0]) # trick to avoid list
        specie = ""
        nameMet = "pseudoSource"
        uniqueId = nameMet +  idMet # trick to avoid list   
    elif splitMet[-1]=="t":
        idMet = "_".join(splitMet[0]) # trick to avoid list
        specie = ""
        nameMet = "pseudoTarget"
        uniqueId = nameMet +  idMet # trick to avoid list   
    else : ## Regular metabolite : we should be able to find its name.
        idMet =  "_".join(splitMet[1:])
        specie = splitMet[0]
        uniqueId = "_".join(splitMet)
        if idMet in mapOfMet.keys():
            nameMet = mapOfMet[idMet]
        else : 
            print "".join(idMet)
            print  "Name not found for " + "".join(idMet) +  " " + "_".join(splitMet)
    myMet = {"idMet": idMet, "idFull" : uniqueId, "name": nameMet, "compartiment" : specie}
    return myMet

def getWholeMetInfo( substrates, products, mapOfMet):
    subs = []
    prods = []
    for sub in substrates:
        myMetSub = getInfo(sub, mapOfMet)
        subs += [myMetSub]
    for prod in products:
        myMetProd = getInfo(prod, mapOfMet)
        prods += [myMetProd]
    return (subs, prods)


# typeR can be : pseudo, transition or ""
def getTypeAndUpdateMetabolite( subs, prods):
    ## Handling special arcs:
    # 3) setting to "transition" the arcs
    typeR = ""
    if (len(subs) ==1 and len(prods) == 1):
        if subs[0]["idMet"] == prods[0]["idMet"]: #either in -> out or transition 
            if not subs[0]["compartiment"] == prods[0]["compartiment"] :
                typeR = "transition"
        if subs[0]["name"] == "pseudoSource" or prods[0]["name"] == "pseudoTarget" :
            typeR = "pseudo"            
    return (typeR)



def creatingSolution(mapOfMet, infoLine, metInSol, reactInSol, reactionsList, subOfReaction,  addedReactions):
    (substrates, products, weight) = infoLine
    (subs, prods) = getWholeMetInfo( substrates, products, mapOfMet)
    (typeR ) = getTypeAndUpdateMetabolite( subs, prods)
    if typeR == "" :
        if  weight > 1: # weight is here a parameter depending on our parameters...
            typeR = "insertion"
        else :
            typeR = "endo"
    if typeR=="transition":
        for k in (subs + prods) :
            metInSol[ k["idFull"] ] = deepcopy(k)
        currentR = [ [s["idFull"] for s in subs], [p["idFull"] for p in prods], weight, typeR ]
        reactInSol.append(deepcopy(currentR)  )
        return
    else: # No transition means we are alays in the same specie
        species = subs[0]["compartiment"]
    if typeR  == "endo" or typeR == "insertion":
        possibleReaction = []  
        for s in subs:
            possibleReaction += subOfReaction[s["idMet"]]
        (isTheSame, chosenReac) = sameReaction ( possibleReaction, subs, prods, reactionsList)
        if isTheSame :    
            idR = chosenReac
        else :
            print "reaction Not Found"
            sys.exit(1)
        allIdsMet = [ m["idMet"] for m in (subs + prods) ]
        initialSubstrates = reactionsList[ idR][0]
        initialProducts = reactionsList[ idR ][1]  
        ## if a metabolite is not prensent in the solution givent by the rainbows program, then it was a cofactor        
        for s in initialSubstrates:
            if s not in allIdsMet:               
                subs +=  [{"idMet": s, "idFull" : s, "name": mapOfMet[s], "compartiment" : species }]
        for p in initialProducts:
            if p not in allIdsMet:
                prods +=  [{"idMet": p, "idFull" : p, "name": mapOfMet[p] , "compartiment" : species } ]              
        for k in (subs + prods) :
            if k["idFull"] not in  metInSol.keys():
                metInSol[k["idFull"]] = deepcopy(k)
        currentR = [[s["idFull"] for s in subs], [p["idFull"] for p in prods], weight, typeR ]
        reactInSol.append(deepcopy(currentR)  )
  
def main(): 

    parser = argparse.ArgumentParser(description='dinghyFormat') #to redo
    
    # ------------------------------------------------------------------------
    #                            Define allowed options
    # ------------------------------------------------------------------------
    # Mandatory# Will only write the optimal values (lower score)
    versionNumber = "0.0.1"
    parser.add_argument('solution', action="store", help="clingoOutput")
    parser.add_argument('edgeFile', action="store", help="initial network EdgeFile")
    parser.add_argument('SBMLFiles', action="store", help="the SBML file separated by space between quote")
    parser.add_argument('--cofactors', action = 'store', dest = 'getCofact', default = False, required = True, 
                        help = "File containing a list of metabolite considered cofactors" )
    parser.add_argument('--outputD', action="store", help="output Directory", required=False, default="outputForDinghy") 
   
    
    options = parser.parse_args()

    #=============== Directory creations
    outDir = options.outputD
    if not os.path.exists( outDir ):
        os.mkdir( outDir )  
    print outDir
    # Map of Met : dict of dict
    ## key : id, value : dict
    ## the dict value contains : "name", and "specie" :  the Species
    
    # This will return a dict, key =id solution, value : array of reaction
    allSolutions = readASPoutput( options.solution) 
    print "Number of Solutions : "  + str( len( allSolutions ) )

    mapOfMet = dict()
    reactionsList = dict() 
    subOfReaction = dict()
    references = options.SBMLFiles.split( " " ) # split by space 
    for fileName in references:
        getIdN(fileName, mapOfMet, reactionsList, subOfReaction)   

    # Reading the cofactor (for visualization )
    allCofactors = readFileInList( options.getCofact  )
    # Reading the network given to ASP 
    allReactions = readTheEdges  ( options.edgeFile   )

    for numSol, arrayReaction  in allSolutions.iteritems():
        newReactionsInSol = [ allReactions[k] for k in arrayReaction  ]
        tutu = open( outDir + "/solution" + str(numSol) + ".txt", "w" ) 
        for k in arrayReaction :
            (subs, prods, ww) = allReactions[k]
            tutu.write (  "\t".join( [k , ",".join(subs) ,  ",".join(prods) ])  + "\n")
        tutu.close()
         #print mapOfMet
        metInSol = dict()
        reactInSol = []
        addedReactions = set()
        for ii in newReactionsInSol: 
            creatingSolution( mapOfMet, ii, metInSol, reactInSol , reactionsList, subOfReaction,  addedReactions )
        writingTheJson( outDir +  "/solution" + str( numSol ) + ".json", metInSol, reactInSol, allCofactors,versionNumber )
   

   

if __name__ == '__main__':
    main()
